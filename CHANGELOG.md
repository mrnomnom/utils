CHANGELOG
=========

* `1.5.0` - Added Nationality-Country map
    * `1.5.2` - Added possibility to build Country from alpha3 country code

* `1.4.0` - Removed `scottish` and `welsh` nationalities

* `1.3.0` - Added IncomingQueue

* `1.2.2` - Added PDOManager and PDOManagerFactory

* `1.1.0` - Added Logger implementation

* `1.0.0` - Enum classes were extracted from main monolith repository
