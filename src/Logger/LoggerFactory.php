<?php

namespace TGF\Util\Logger;

use Monolog\Logger as LogWriter;

class LoggerFactory
{
    /**
     * @param int    $level
     * @param string $channel
     *
     * @return LogWriter
     * @throws \Exception
     */
    public static function createMonolog(int $level, string $channel): LogWriter
    {
        $streamHandler = new \Monolog\Handler\StreamHandler('php://stdout', $level);
        $streamHandler->setFormatter(new \Monolog\Formatter\JsonFormatter());

        $monolog = new LogWriter(strtolower($channel));
        $monolog->pushHandler($streamHandler);

        return $monolog;
    }

    /**
     * @param LogWriter       $logger
     * @param MetaProvider $metaProvider
     *
     * @return LoggerInterface
     */
    public static function create(LogWriter $logger, MetaProvider $metaProvider): LoggerInterface
    {
        return new Logger($logger, $metaProvider);
    }
}
