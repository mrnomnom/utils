<?php

namespace TGF\Util\Logger;

interface LogDecoratorInterface
{
    /**
     * @param array $record
     *
     * @return array
     */
    public function decorate(array $record): array;
}
