<?php

namespace TGF\Util\Logger;

class MetaProvider
{
    const LENGTH = 16;

    /** @var array */
    private $cached;

    /** @var string */
    private $project;

    /** @var string */
    private $build;

    /**
     * @param string $project
     * @param string $build
     */
    public function __construct(string $project, string $build = null)
    {
        $this->project = $project;
        $this->build = $build;
    }

    /**
     * @return array
     */
    public function getMeta(): array
    {
        if (!$this->cached) {
            $this->cacheMeta();
        }

        return array_merge(
            $this->cached,
            [
                'happened_at' => (new \DateTime())->format("c"),
            ]
        );
    }

    /**
     * @param string $traceId
     */
    public function setTraceId(string $traceId)
    {
        if (!$this->cached) {
            $this->cacheMeta();
        }

        $this->cached['traceId'] = $traceId;
    }

    /**
     * @return string
     */
    public function getTraceId(): string
    {
        if (!$this->cached) {
            $this->cacheMeta();
        }

        return $this->cached['traceId'];
    }


    public function refreshTraceId()
    {
        if (!$this->cached) {
            $this->cacheMeta();
        }

        $this->cached['traceId'] = $this->generateTraceId();
    }

    /**
     * @return string
     */
    private function generateTraceId(): string
    {
        return bin2hex(random_bytes(self::LENGTH));
    }

    private function cacheMeta()
    {
        $this->cached = [
            'traceId' => $this->generateTraceId(),
            'build'   => $this->build,
            'host'    => gethostname(),
            'project' => $this->project,
        ];
    }
}
