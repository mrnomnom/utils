<?php declare(strict_types=1);

namespace TGF\Util\Logger\Processor;

use TGF\Util\Logger\LogDecoratorInterface;

class CustomProcessor
{
    /** @var LogDecoratorInterface[] */
    private $logDecorators;

    /**
     * @param LogDecoratorInterface[] $logDecorators
     */
    public function __construct(array $logDecorators)
    {
        $this->logDecorators = $logDecorators;
    }

    /**
     * @param array $record
     *
     * @return array
     */
    public function __invoke(array $record): array
    {
        foreach($this->logDecorators as $logDecorator) {

            try {
                $record = $logDecorator->decorate($record);
            } catch(\Throwable $e) {
                // Preventing faulty decorator from corrupting logger
            }
        }

        return $record;
    }
}
