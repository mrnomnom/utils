<?php declare(strict_types=1);

namespace TGF\Util\Logger\Processor;

use TGF\Util\Logger\MetaProvider;

class MetaProcessor
{
    /** @var MetaProvider */
    private $metaProvider;

    /**
     * @param MetaProvider $metaProvider
     */
    public function __construct(MetaProvider $metaProvider)
    {
        $this->metaProvider = $metaProvider;
    }

    /**
     * @param array $record
     *
     * @return array
     */
    public function __invoke(array $record): array
    {
        $record['meta'] = $this->metaProvider->getMeta();

        return $record;
    }
}
