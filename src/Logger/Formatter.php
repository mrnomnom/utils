<?php

namespace TGF\Util\Logger;

class Formatter
{
    /**
     * @param $args
     *
     * @return array
     * @throws \ReflectionException
     */
    public function format(array $args): array
    {
        $result = ['context' => []];

        foreach ($args as $param) {
            if (\is_string($param)) {
                if (!isset($result['message'])) {
                    $result['message'] = $param;
                } else {
                    $result['context']['message'] = $param;
                }
            }

            if ($param instanceof \Throwable) {
                if (!isset($result['message'])) {
                    $message = $param->getMessage();
                    if (!$message) {
                        $reflect = new \ReflectionClass($param);
                        $message = $reflect->getShortName();
                    }
                    $result['message'] = $message;
                } else {
                    $result['context']['message'] = $param->getMessage();
                }

                $result['context']['stacktrace'] = $param->getTraceAsString();
            }

            if (\is_array($param)) {
                $result['context'] = array_merge($result['context'], $param);
            }
        }

        $result['context'] = $this->sanitize_context($result['context']);

        return $result;
    }

    /**
     * @param $payload
     *
     * @return array
     */
    private function sanitize_context($payload): array
    {
        if (empty($payload)) {
            return $payload;
        }
        if (json_encode($payload) === false) {
            return ['_NON_SERIALIZABLE_' => base64_encode(serialize($payload))];
        }

        return $payload;
    }
}
