<?php

namespace TGF\Util\Logger;

interface LoggerInterface
{
    /**
     * @param string|\Exception $message
     * @param array             $context
     */
    public function warning($_);

    /**
     * @param string|\Exception $message
     * @param array             $context
     */
    public function error($_);

    /**
     * @param string|\Exception $message
     * @param array             $context
     */
    public function info($_);

    /**
     * @param string|\Exception $message
     * @param array             $context
     */
    public function debug($_);
}
