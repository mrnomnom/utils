<?php

namespace TGF\Util\Logger;

use Monolog\Logger as LogWriter;
use TGF\Util\Logger\Processor\CustomProcessor;
use TGF\Util\Logger\Processor\MetaProcessor;

class Logger implements LoggerInterface
{
    /** @var MetaProvider */
    private $metaProvider;

    /** @var LogWriter */
    private $logWriter;

    /** @var Formatter */
    private $formatter;

    /**
     * @param LogWriter               $logWriter
     * @param MetaProvider            $metaProvider
     * @param LogDecoratorInterface[] $logDecorators
     */
    public function __construct(
        LogWriter $logWriter,
        MetaProvider $metaProvider,
        array $logDecorators = []
    ) {
        $this->logWriter = $logWriter;
        $this->metaProvider = $metaProvider;
        $this->formatter = new Formatter();

        $this->configureLogWriter($logDecorators);
    }

    public function error($_)
    {
        $_;
        $args = func_get_args();

        $logData = $this->formatter->format($args);

        $this->logWriter->error($logData['message'], $logData['context']);
    }

    public function warning($_)
    {
        $_;
        $args = func_get_args();

        $logData = $this->formatter->format($args);

        $this->logWriter->warning($logData['message'], $logData['context']);
    }

    public function info($_)
    {
        $_;
        $args = func_get_args();

        $logData = $this->formatter->format($args);

        $this->logWriter->info($logData['message'], $logData['context']);
    }

    public function debug($_)
    {
        $_;
        $args = func_get_args();

        $logData = $this->formatter->format($args);

        $this->logWriter->debug($logData['message'], $logData['context']);
    }

    /**
     * @param LogDecoratorInterface[] $logDecorators
     */
    private function configureLogWriter(array $logDecorators)
    {
        $metaProcessor = new MetaProcessor($this->metaProvider);
        $customProcessor = new CustomProcessor($logDecorators);

        $this->logWriter->pushProcessor($metaProcessor);
        $this->logWriter->pushProcessor($customProcessor);
    }
}
