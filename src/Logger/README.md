Logger Implementation
=======================

TransferGo logger implementation.

## Setup

1 - Initialize Monolog
2 - Initialize MetaProvider

```php
$metaProvider = new TGF\Util\Logger\MetaProvider(
    string $project,
    string $build
);
``` 

3 - (optional) Implement custom log message decorators (LogDecoratorInterface)

4 - Initialize Logger class

```php
$logger = new TGF\Util\Logger\Logger(
    Monolog\Logger $logger,
    MetaProvider $metaProvider,
    array $contextDecorators
);
```

5 - Start logging!

### Decorating Log messages

To decorate log messages, implement custom decorators (LogDecoratorInterface) and pass them to Logger via constructor.

Example:
```php
// Implements LogDecoratorInterface
$decorator = new MyCustomDecorator(); 

$logger = new TGF\Util\Logger\Logger(
    Monolog\Logger $logger,
    MetaProvider $metaProvider,
    [ $myDecorator ]
);
```

### Working with Trace ID

In order to pass Trace ID from Logger to a published message or request and vice versa,
use MetaProvider class getTraceId() and setTraceId() methods.