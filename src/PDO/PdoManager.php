<?php

namespace TGF\Util\PDO;

use TGF\Util\Logger\LoggerInterface;

/**
 * @method \PDOStatement prepare ($statement, array $driver_options = array())
 * @method \PDOStatement query ($statement, $mode = \PDO::ATTR_DEFAULT_FETCH_MODE, $arg3 = null, array $ctorargs = array())
 * @method int exec ($statement)
 * @method string lastInsertId ($name = null)
 * @method bool beginTransaction ()
 * @method bool commit ()
 * @method bool rollBack ()
 * @method bool inTransaction ()
 * @method bool setAttribute ($attribute, $value)
 * @method mixed getAttribute ($attribute)
 * @method string quote ($string, $parameter_type = \PDO::PARAM_STR)
 * @method mixed errorCode ()
 * @method array errorInfo ()
 * @method array getAvailableDrivers ()
 */
class PDOManager
{
    const RETRY_ATTEMPTS = 3;

    /** @var string */
    private $dsn;

    /** @var string */
    private $username;

    /** @var string */
    private $password;

    /** @var array */
    private $options;

    /** @var \PDO */
    private $db;

    /** @var LoggerInterface */
    private $logger;

    /**
     * @param string          $dsn
     * @param string          $username
     * @param string          $password
     * @param array           $options
     * @param LoggerInterface $logger
     */
    public function __construct(
        string $dsn,
        string $username,
        string $password,
        $options = [],
        LoggerInterface $logger
    ) {
        $this->dsn = $dsn;
        $this->username = $username;
        $this->password = $password;
        $this->options = $options;
        $this->logger = $logger;

        $this->connect();
    }

    /**
     * @param $method
     * @param $args
     *
     * @return mixed
     * @throws PdoManagerException
     */
    public function __call($method, $args)
    {
        $this->logger->debug('PDO_MANAGER_CALL_INIT', array_merge(['methodName' => $method], $args));

        try {
            return $this->doCall($method, $args);
        } catch (\Throwable $e) {
            throw new PdoManagerException($e);
        }
    }

    /**
     * @param $method
     * @param $args
     *
     * @return mixed
     * @throws \Exception
     */
    private function doCall($method, $args) {

        $hasGoneAway = false;
        $retryAttempt = 0;

        try_again:
        try {
            if ($this->db === null) {
                $retryAttempt++;
                $this->tryToRecoverFromNullDB();
            }

            $this->db->query("SELECT 1"); // Used to prevent PDOStatement->execute() from failing

            if (is_callable([$this->db, $method])) {
                return call_user_func_array([$this->db, $method], $args);
            }

            throw new \Exception("Call to undefined method '{$method}'");
        } catch (\PDOException $e) {
            $exceptionMessage = $e->getMessage();
            $this->logger->debug('PDO_MANAGER_EXCEPTION', ['error' => $exceptionMessage]);
            $isGoneAway = strpos($exceptionMessage, 'server has gone away') !== false;

            if ($isGoneAway && $retryAttempt <= self::RETRY_ATTEMPTS) {
                $this->logger->debug('PDO_MANAGER_GONE_AWAY');
                $hasGoneAway = true;
            } else {
                if ($isGoneAway) {
                    $this->logger->error('PDO_MANAGER_EXCEEDED_MAX_RECONNECTIONS');
                }

                throw $e;
            }
        }

        if ($hasGoneAway) {
            $this->logger->debug('PDO_MANAGER_ATTEMPTS_RECONNECT');
            $retryAttempt++;
            $this->reconnect();
            goto try_again;
        }
    }

    /**
     * Connects to DB
     */
    private function connect()
    {
        $this->db = new \PDO($this->dsn, $this->username, $this->password, $this->options);
        $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->logger->debug('PDO_MANAGER_CONNECT');
    }

    /**
     * @throws \Exception
     */
    private function tryToRecoverFromNullDB()
    {
        $this->reconnect();

        if ($this->db === null) {
            throw new \Exception('After connect database was null');
        }
    }

    /**
     * Reconnects to DB
     */
    private function reconnect()
    {
        $this->db = null;
        $this->connect();
    }
}
