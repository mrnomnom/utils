<?php

namespace TGF\Util\PDO;

use Throwable;

class PdoManagerException extends \Exception
{
    /** @var Throwable */
    private $original;

    /**
     * @param Throwable $original
     */
    public function __construct(Throwable $original)
    {
        parent::__construct($original->getMessage());
        $this->original = $original;
    }

    /**
     * @return Throwable
     */
    public function getOriginalException(): Throwable
    {
        return $this->original;
    }
}
