<?php

namespace TGF\Util\PDO;

use TGF\Util\Logger\LoggerInterface;

class PDOManagerFactory
{
    /**
     * @param LoggerInterface $logger
     * @param string          $hostname
     * @param string          $db
     * @param string          $user
     * @param string          $password
     *
     * @return PDOManager
     * @throws \Exception
     */
    public static function create(
        LoggerInterface $logger,
        string $hostname,
        string $db,
        string $user,
        string $password
    ): PDOManager {
        try {
            $dsn = sprintf(
                'mysql:host=%s;dbname=%s',
                $hostname,
                $db
            );

            return new PDOManager(
                $dsn,
                $user,
                $password,
                [\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"],
                $logger
            );
        } catch (\PDOException $e) {
            throw new \Exception("Can't connect to database. " . $e->getMessage() . ". Using: " . $dsn . " DSN");
        }
    }
}
