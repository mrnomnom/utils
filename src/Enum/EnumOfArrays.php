<?php

namespace TGF\Util\Enum;

abstract class EnumOfArrays
{
    /**
     * @var string
     */
    private $key;

    /**
     * Store existing constants in a static cache per object.
     *
     * @var array
     */
    private static $cache = array();

    public function __construct($key)
    {
        if (!isset(self::toArray()[$key])) {
            $key = strtoupper($key);
        }

        if (!isset(self::toArray()[$key])) {
            throw new \BadMethodCallException(sprintf('Unknown enum key "%s"', $key));
        }

        $this->key = $key;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->key;
    }

    /**
     * @return array
     * @throws \LogicException
     */
    public static function toArray()
    {
        $class = get_called_class();
        if (!array_key_exists($class, self::$cache)) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $reflection = new \ReflectionClass($class);

            if (!isset($reflection->getStaticProperties()['values'])) {
                throw new \LogicException('Enum class must have static property called value');
            }

            self::$cache[$class] = $reflection->getStaticProperties()['values'];
        }

        return self::$cache[$class];
    }

    /**
     * Returns the name that is suitable for displaying this value.
     *
     * @return string
     */
    public function getDisplayName()
    {
        return self::toArray()[$this->key]['display_name'];
    }

    /**
     * @param $name
     * @param $arguments
     * @return static
     */
    public static function __callStatic($name, $arguments)
    {
        // PHPUnit coverage will fail if $arguments is not defined and CodeSniffer complains about unused variable :/
        $arguments;
        return new static($name);
    }
}
