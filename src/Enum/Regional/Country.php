<?php

namespace TGF\Util\Enum\Regional;

use TGF\Util\Enum\EnumOfArrays;

/**
 * @method static Country AF()
 * @method static Country AX()
 * @method static Country AL()
 * @method static Country DZ()
 * @method static Country AS()
 * @method static Country AD()
 * @method static Country AO()
 * @method static Country AI()
 * @method static Country AQ()
 * @method static Country AG()
 * @method static Country AR()
 * @method static Country AM()
 * @method static Country AW()
 * @method static Country AU()
 * @method static Country AT()
 * @method static Country AZ()
 * @method static Country BS()
 * @method static Country BH()
 * @method static Country BD()
 * @method static Country BB()
 * @method static Country BY()
 * @method static Country BE()
 * @method static Country BZ()
 * @method static Country BJ()
 * @method static Country BM()
 * @method static Country BT()
 * @method static Country BO()
 * @method static Country BQ()
 * @method static Country BA()
 * @method static Country BW()
 * @method static Country BV()
 * @method static Country BR()
 * @method static Country IO()
 * @method static Country BN()
 * @method static Country BG()
 * @method static Country BF()
 * @method static Country BI()
 * @method static Country KH()
 * @method static Country CM()
 * @method static Country CA()
 * @method static Country CV()
 * @method static Country KY()
 * @method static Country CF()
 * @method static Country TD()
 * @method static Country CL()
 * @method static Country CN()
 * @method static Country CX()
 * @method static Country CC()
 * @method static Country CO()
 * @method static Country KM()
 * @method static Country CG()
 * @method static Country CD()
 * @method static Country CK()
 * @method static Country CR()
 * @method static Country CI()
 * @method static Country HR()
 * @method static Country CU()
 * @method static Country CW()
 * @method static Country CY()
 * @method static Country CZ()
 * @method static Country DK()
 * @method static Country DJ()
 * @method static Country DM()
 * @method static Country DO()
 * @method static Country EC()
 * @method static Country EG()
 * @method static Country SV()
 * @method static Country GQ()
 * @method static Country ER()
 * @method static Country EE()
 * @method static Country ET()
 * @method static Country FK()
 * @method static Country FO()
 * @method static Country FJ()
 * @method static Country FI()
 * @method static Country FR()
 * @method static Country GF()
 * @method static Country PF()
 * @method static Country TF()
 * @method static Country GA()
 * @method static Country GM()
 * @method static Country GE()
 * @method static Country DE()
 * @method static Country GH()
 * @method static Country GI()
 * @method static Country GR()
 * @method static Country GL()
 * @method static Country GD()
 * @method static Country GP()
 * @method static Country GU()
 * @method static Country GT()
 * @method static Country GG()
 * @method static Country GN()
 * @method static Country GW()
 * @method static Country GY()
 * @method static Country HT()
 * @method static Country HM()
 * @method static Country HN()
 * @method static Country HK()
 * @method static Country HU()
 * @method static Country IS()
 * @method static Country IN()
 * @method static Country ID()
 * @method static Country IR()
 * @method static Country IQ()
 * @method static Country IE()
 * @method static Country IM()
 * @method static Country IL()
 * @method static Country IT()
 * @method static Country JM()
 * @method static Country JP()
 * @method static Country JE()
 * @method static Country JO()
 * @method static Country KZ()
 * @method static Country KE()
 * @method static Country KI()
 * @method static Country KP()
 * @method static Country KR()
 * @method static Country KW()
 * @method static Country KG()
 * @method static Country LA()
 * @method static Country LV()
 * @method static Country LB()
 * @method static Country LS()
 * @method static Country LR()
 * @method static Country LY()
 * @method static Country LI()
 * @method static Country LT()
 * @method static Country LU()
 * @method static Country MO()
 * @method static Country MK()
 * @method static Country MG()
 * @method static Country MW()
 * @method static Country MY()
 * @method static Country MV()
 * @method static Country ML()
 * @method static Country MT()
 * @method static Country MH()
 * @method static Country MQ()
 * @method static Country MR()
 * @method static Country MU()
 * @method static Country YT()
 * @method static Country MX()
 * @method static Country FM()
 * @method static Country MD()
 * @method static Country MC()
 * @method static Country MN()
 * @method static Country ME()
 * @method static Country MS()
 * @method static Country MA()
 * @method static Country MZ()
 * @method static Country MM()
 * @method static Country NA()
 * @method static Country NR()
 * @method static Country NP()
 * @method static Country NL()
 * @method static Country AN()
 * @method static Country NC()
 * @method static Country NZ()
 * @method static Country NI()
 * @method static Country NE()
 * @method static Country NG()
 * @method static Country NU()
 * @method static Country NF()
 * @method static Country MP()
 * @method static Country NO()
 * @method static Country OM()
 * @method static Country PK()
 * @method static Country PW()
 * @method static Country PS()
 * @method static Country PA()
 * @method static Country PG()
 * @method static Country PY()
 * @method static Country PE()
 * @method static Country PH()
 * @method static Country PN()
 * @method static Country PL()
 * @method static Country PT()
 * @method static Country PR()
 * @method static Country QA()
 * @method static Country RE()
 * @method static Country RO()
 * @method static Country RU()
 * @method static Country RW()
 * @method static Country BL()
 * @method static Country SH()
 * @method static Country KN()
 * @method static Country LC()
 * @method static Country MF()
 * @method static Country PM()
 * @method static Country VC()
 * @method static Country WS()
 * @method static Country SM()
 * @method static Country ST()
 * @method static Country SA()
 * @method static Country SN()
 * @method static Country RS()
 * @method static Country SC()
 * @method static Country SL()
 * @method static Country SG()
 * @method static Country SX()
 * @method static Country SK()
 * @method static Country SI()
 * @method static Country SB()
 * @method static Country SO()
 * @method static Country ZA()
 * @method static Country GS()
 * @method static Country SS()
 * @method static Country ES()
 * @method static Country LK()
 * @method static Country SD()
 * @method static Country SR()
 * @method static Country SJ()
 * @method static Country SZ()
 * @method static Country SE()
 * @method static Country CH()
 * @method static Country SY()
 * @method static Country TW()
 * @method static Country TJ()
 * @method static Country TZ()
 * @method static Country TH()
 * @method static Country TL()
 * @method static Country TG()
 * @method static Country TK()
 * @method static Country TO()
 * @method static Country TT()
 * @method static Country TN()
 * @method static Country TR()
 * @method static Country TM()
 * @method static Country TC()
 * @method static Country TV()
 * @method static Country UG()
 * @method static Country UA()
 * @method static Country AE()
 * @method static Country GB()
 * @method static Country UM()
 * @method static Country US()
 * @method static Country UY()
 * @method static Country UZ()
 * @method static Country VU()
 * @method static Country VA()
 * @method static Country VE()
 * @method static Country VN()
 * @method static Country VG()
 * @method static Country VI()
 * @method static Country WF()
 * @method static Country EH()
 * @method static Country YE()
 * @method static Country ZM()
 * @method static Country ZW()
 */
class Country extends EnumOfArrays
{
    const AF = 'AF';
    const AX = 'AX';
    const AL = 'AL';
    const DZ = 'DZ';
    const AS = 'AS';
    const AD = 'AD';
    const AO = 'AO';
    const AI = 'AI';
    const AQ = 'AQ';
    const AG = 'AG';
    const AR = 'AR';
    const AM = 'AM';
    const AW = 'AW';
    const AU = 'AU';
    const AT = 'AT';
    const AZ = 'AZ';
    const BS = 'BS';
    const BH = 'BH';
    const BD = 'BD';
    const BB = 'BB';
    const BY = 'BY';
    const BE = 'BE';
    const BZ = 'BZ';
    const BJ = 'BJ';
    const BM = 'BM';
    const BT = 'BT';
    const BO = 'BO';
    const BQ = 'BQ';
    const BA = 'BA';
    const BW = 'BW';
    const BV = 'BV';
    const BR = 'BR';
    const IO = 'IO';
    const BN = 'BN';
    const BG = 'BG';
    const BF = 'BF';
    const BI = 'BI';
    const KH = 'KH';
    const CM = 'CM';
    const CA = 'CA';
    const CV = 'CV';
    const KY = 'KY';
    const CF = 'CF';
    const TD = 'TD';
    const CL = 'CL';
    const CN = 'CN';
    const CX = 'CX';
    const CC = 'CC';
    const CO = 'CO';
    const KM = 'KM';
    const CG = 'CG';
    const CD = 'CD';
    const CK = 'CK';
    const CR = 'CR';
    const CI = 'CI';
    const HR = 'HR';
    const CU = 'CU';
    const CW = 'CW';
    const CY = 'CY';
    const CZ = 'CZ';
    const DK = 'DK';
    const DJ = 'DJ';
    const DM = 'DM';
    const DO = 'DO';
    const EC = 'EC';
    const EG = 'EG';
    const SV = 'SV';
    const GQ = 'GQ';
    const ER = 'ER';
    const EE = 'EE';
    const ET = 'ET';
    const FK = 'FK';
    const FO = 'FO';
    const FJ = 'FJ';
    const FI = 'FI';
    const FR = 'FR';
    const GF = 'GF';
    const PF = 'PF';
    const TF = 'TF';
    const GA = 'GA';
    const GM = 'GM';
    const GE = 'GE';
    const DE = 'DE';
    const GH = 'GH';
    const GI = 'GI';
    const GR = 'GR';
    const GL = 'GL';
    const GD = 'GD';
    const GP = 'GP';
    const GU = 'GU';
    const GT = 'GT';
    const GG = 'GG';
    const GN = 'GN';
    const GW = 'GW';
    const GY = 'GY';
    const HT = 'HT';
    const HM = 'HM';
    const HN = 'HN';
    const HK = 'HK';
    const HU = 'HU';
    const IS = 'IS';
    const IN = 'IN';
    const ID = 'ID';
    const IR = 'IR';
    const IQ = 'IQ';
    const IE = 'IE';
    const IM = 'IM';
    const IL = 'IL';
    const IT = 'IT';
    const JM = 'JM';
    const JP = 'JP';
    const JE = 'JE';
    const JO = 'JO';
    const KZ = 'KZ';
    const KE = 'KE';
    const KI = 'KI';
    const KP = 'KP';
    const KR = 'KR';
    const KW = 'KW';
    const KG = 'KG';
    const LA = 'LA';
    const LV = 'LV';
    const LB = 'LB';
    const LS = 'LS';
    const LR = 'LR';
    const LY = 'LY';
    const LI = 'LI';
    const LT = 'LT';
    const LU = 'LU';
    const MO = 'MO';
    const MK = 'MK';
    const MG = 'MG';
    const MW = 'MW';
    const MY = 'MY';
    const MV = 'MV';
    const ML = 'ML';
    const MT = 'MT';
    const MH = 'MH';
    const MQ = 'MQ';
    const MR = 'MR';
    const MU = 'MU';
    const YT = 'YT';
    const MX = 'MX';
    const FM = 'FM';
    const MD = 'MD';
    const MC = 'MC';
    const MN = 'MN';
    const ME = 'ME';
    const MS = 'MS';
    const MA = 'MA';
    const MZ = 'MZ';
    const MM = 'MM';
    const NA = 'NA';
    const NR = 'NR';
    const NP = 'NP';
    const NL = 'NL';
    const AN = 'AN';
    const NC = 'NC';
    const NZ = 'NZ';
    const NI = 'NI';
    const NE = 'NE';
    const NG = 'NG';
    const NU = 'NU';
    const NF = 'NF';
    const MP = 'MP';
    const NO = 'NO';
    const OM = 'OM';
    const PK = 'PK';
    const PW = 'PW';
    const PS = 'PS';
    const PA = 'PA';
    const PG = 'PG';
    const PY = 'PY';
    const PE = 'PE';
    const PH = 'PH';
    const PN = 'PN';
    const PL = 'PL';
    const PT = 'PT';
    const PR = 'PR';
    const QA = 'QA';
    const RE = 'RE';
    const RO = 'RO';
    const RU = 'RU';
    const RW = 'RW';
    const BL = 'BL';
    const SH = 'SH';
    const KN = 'KN';
    const LC = 'LC';
    const MF = 'MF';
    const PM = 'PM';
    const VC = 'VC';
    const WS = 'WS';
    const SM = 'SM';
    const ST = 'ST';
    const SA = 'SA';
    const SN = 'SN';
    const RS = 'RS';
    const SC = 'SC';
    const SL = 'SL';
    const SG = 'SG';
    const SX = 'SX';
    const SK = 'SK';
    const SI = 'SI';
    const SB = 'SB';
    const SO = 'SO';
    const ZA = 'ZA';
    const GS = 'GS';
    const SS = 'SS';
    const ES = 'ES';
    const LK = 'LK';
    const SD = 'SD';
    const SR = 'SR';
    const SJ = 'SJ';
    const SZ = 'SZ';
    const SE = 'SE';
    const CH = 'CH';
    const SY = 'SY';
    const TW = 'TW';
    const TJ = 'TJ';
    const TZ = 'TZ';
    const TH = 'TH';
    const TL = 'TL';
    const TG = 'TG';
    const TK = 'TK';
    const TO = 'TO';
    const TT = 'TT';
    const TN = 'TN';
    const TR = 'TR';
    const TM = 'TM';
    const TC = 'TC';
    const TV = 'TV';
    const UG = 'UG';
    const UA = 'UA';
    const AE = 'AE';
    const GB = 'GB';
    const UM = 'UM';
    const US = 'US';
    const UY = 'UY';
    const UZ = 'UZ';
    const VU = 'VU';
    const VA = 'VA';
    const VE = 'VE';
    const VN = 'VN';
    const VG = 'VG';
    const VI = 'VI';
    const WF = 'WF';
    const EH = 'EH';
    const YE = 'YE';
    const ZM = 'ZM';
    const ZW = 'ZW';

    /** @var array */
    private static $values = [
        self::AF => [
            'display_name' => 'Afghanistan',
            'alpha2' => self::AF,
            'alpha3' => 'AFG',
            'numeric_code' => '004'
        ],
        self::AX => [
            'display_name' => 'Åland',
            'alpha2' => self::AX,
            'alpha3' => 'ALA',
            'numeric_code' => '248'
        ],
        self::AL => [
            'display_name' => 'Albania',
            'alpha2' => self::AL,
            'alpha3' => 'ALB',
            'numeric_code' => '008'
        ],
        self::DZ => [
            'display_name' => 'Algeria',
            'alpha2' => self::DZ,
            'alpha3' => 'DZA',
            'numeric_code' => '012'
        ],
        self::AS => [
            'display_name' => 'American Samoa',
            'alpha2' => self::AS,
            'alpha3' => 'ASM',
            'numeric_code' => '016'
        ],
        self::AD => [
            'display_name' => 'Andorra',
            'alpha2' => self::AD,
            'alpha3' => 'AND',
            'numeric_code' => '020'
        ],
        self::AO => [
            'display_name' => 'Angola',
            'alpha2' => self::AO,
            'alpha3' => 'AGO',
            'numeric_code' => '024'
        ],
        self::AI => [
            'display_name' => 'Anguilla',
            'alpha2' => self::AI,
            'alpha3' => 'AIA',
            'numeric_code' => '660'
        ],
        self::AQ => [
            'display_name' => 'Antarctica',
            'alpha2' => self::AQ,
            'alpha3' => 'ATA',
            'numeric_code' => '010'
        ],
        self::AG => [
            'display_name' => 'Antigua and Barbuda',
            'alpha2' => self::AG,
            'alpha3' => 'ATG',
            'numeric_code' => '028'
        ],
        self::AR => [
            'display_name' => 'Argentina',
            'alpha2' => self::AR,
            'alpha3' => 'ARG',
            'numeric_code' => '032'
        ],
        self::AM => [
            'display_name' => 'Armenia',
            'alpha2' => self::AM,
            'alpha3' => 'ARM',
            'numeric_code' => '051'
        ],
        self::AW => [
            'display_name' => 'Aruba',
            'alpha2' => self::AW,
            'alpha3' => 'ABW',
            'numeric_code' => '051'
        ],
        self::AU => [
            'display_name' => 'Australia',
            'alpha2' => self::AU,
            'alpha3' => 'AUS',
            'numeric_code' => '036'
        ],
        self::AT => [
            'display_name' => 'Austria',
            'alpha2' => self::AT,
            'alpha3' => 'AUT',
            'numeric_code' => '040'
        ],
        self::AZ => [
            'display_name' => 'Azerbaijan',
            'alpha2' => self::AZ,
            'alpha3' => 'AZE',
            'numeric_code' => '031'
        ],
        self::BS => [
            'display_name' => 'Bahamas',
            'alpha2' => self::BS,
            'alpha3' => 'BHS',
            'numeric_code' => '044'
        ],
        self::BH => [
            'display_name' => 'Bahrain',
            'alpha2' => self::BH,
            'alpha3' => 'BHR',
            'numeric_code' => '048'
        ],
        self::BD => [
            'display_name' => 'Bangladesh',
            'alpha2' => self::BD,
            'alpha3' => 'BGD',
            'numeric_code' => '050'
        ],
        self::BB => [
            'display_name' => 'Barbados',
            'alpha2' => self::BB,
            'alpha3' => 'BRB',
            'numeric_code' => '052'
        ],
        self::BY => [
            'display_name' => 'Belarus',
            'alpha2' => self::BY,
            'alpha3' => 'BLR',
            'numeric_code' => '112'
        ],
        self::BE => [
            'display_name' => 'Belgium',
            'alpha2' => self::BE,
            'alpha3' => 'BEL',
            'numeric_code' => '056'
        ],
        self::BZ => [
            'display_name' => 'Belize',
            'alpha2' => self::BZ,
            'alpha3' => 'BLZ',
            'numeric_code' => '084'
        ],
        self::BJ => [
            'display_name' => 'Benin',
            'alpha2' => self::BJ,
            'alpha3' => 'BEN',
            'numeric_code' => '204'
        ],
        self::BM => [
            'display_name' => 'Bermuda',
            'alpha2' => self::BM,
            'alpha3' => 'BMU',
            'numeric_code' => '060'
        ],
        self::BT => [
            'display_name' => 'Bhutan',
            'alpha2' => self::BT,
            'alpha3' => 'BTN',
            'numeric_code' => '064'
        ],
        self::BO => [
            'display_name' => 'Bolivia',
            'alpha2' => self::BO,
            'alpha3' => 'BOL',
            'numeric_code' => '068'
        ],
        self::BQ => [
            'display_name' => 'Bonaire, Sint Eustatius and Saba',
            'alpha2' => self::BQ,
            'alpha3' => 'BES',
            'numeric_code' => '535'
        ],
        self::BA => [
            'display_name' => 'Bosnia and Herzegovina',
            'alpha2' => self::BA,
            'alpha3' => 'BIH',
            'numeric_code' => '070'
        ],
        self::BW => [
            'display_name' => 'Botswana',
            'alpha2' => self::BW,
            'alpha3' => 'BWA',
            'numeric_code' => '072'
        ],
        self::BV => [
            'display_name' => 'Bouvet Island',
            'alpha2' => self::BV,
            'alpha3' => 'BVT',
            'numeric_code' => '074'
        ],
        self::BR => [
            'display_name' => 'Brazil',
            'alpha2' => self::BR,
            'alpha3' => 'BRA',
            'numeric_code' => '076'
        ],
        self::IO => [
            'display_name' => 'British Indian Ocean Territory',
            'alpha2' => self::IO,
            'alpha3' => 'IOT',
            'numeric_code' => '086'
        ],
        self::BN => [
            'display_name' => 'Brunei Darussalam',
            'alpha2' => self::BN,
            'alpha3' => 'BRN',
            'numeric_code' => '096'
        ],
        self::BG => [
            'display_name' => 'Bulgaria',
            'alpha2' => self::BG,
            'alpha3' => 'BGR',
            'numeric_code' => '100'
        ],
        self::BF => [
            'display_name' => 'Burkina Faso',
            'alpha2' => self::BF,
            'alpha3' => 'BFA',
            'numeric_code' => '854'
        ],
        self::BI => [
            'display_name' => 'Burundi',
            'alpha2' => self::BI,
            'alpha3' => 'BDI',
            'numeric_code' => '108'
        ],
        self::KH => [
            'display_name' => 'Cambodia',
            'alpha2' => self::KH,
            'alpha3' => 'KHM',
            'numeric_code' => '116'
        ],
        self::CM => [
            'display_name' => 'Cameroon',
            'alpha2' => self::CM,
            'alpha3' => 'CMR',
            'numeric_code' => '120'
        ],
        self::CA => [
            'display_name' => 'Canada',
            'alpha2' => self::CA,
            'alpha3' => 'CAN',
            'numeric_code' => '124'
        ],
        self::CV => [
            'display_name' => 'Cape Verde',
            'alpha2' => self::CV,
            'alpha3' => 'CPV',
            'numeric_code' => '132'
        ],
        self::KY => [
            'display_name' => 'Cayman Islands',
            'alpha2' => self::KY,
            'alpha3' => 'CYM',
            'numeric_code' => '136'
        ],
        self::CF => [
            'display_name' => 'Central African Republic',
            'alpha2' => self::CF,
            'alpha3' => 'CAF',
            'numeric_code' => '140'
        ],
        self::TD => [
            'display_name' => 'Chad',
            'alpha2' => self::TD,
            'alpha3' => 'TCD',
            'numeric_code' => '148'
        ],
        self::CL => [
            'display_name' => 'Chile',
            'alpha2' => self::CL,
            'alpha3' => 'CHL',
            'numeric_code' => '152'
        ],
        self::CN => [
            'display_name' => 'China',
            'alpha2' => self::CN,
            'alpha3' => 'CHN',
            'numeric_code' => '156'
        ],
        self::CX => [
            'display_name' => 'Christmas Island',
            'alpha2' => self::CX,
            'alpha3' => 'CXR',
            'numeric_code' => '162'
        ],
        self::CC => [
            'display_name' => 'Cocos (Keeling) Islands',
            'alpha2' => self::CC,
            'alpha3' => 'CCK',
            'numeric_code' => '166'
        ],
        self::CO => [
            'display_name' => 'Colombia',
            'alpha2' => self::CO,
            'alpha3' => 'COL',
            'numeric_code' => '170'
        ],
        self::KM => [
            'display_name' => 'Comoros',
            'alpha2' => self::KM,
            'alpha3' => 'COM',
            'numeric_code' => '174'
        ],
        self::CG => [
            'display_name' => 'Congo (Brazzaville)',
            'alpha2' => self::CG,
            'alpha3' => 'COG',
            'numeric_code' => '178'
        ],
        self::CD => [
            'display_name' => 'Congo (Kinshasa)',
            'alpha2' => self::CD,
            'alpha3' => 'COD',
            'numeric_code' => '180'
        ],
        self::CK => [
            'display_name' => 'Cook Islands',
            'alpha2' => self::CK,
            'alpha3' => 'COK',
            'numeric_code' => '184'
        ],
        self::CR => [
            'display_name' => 'Costa Rica',
            'alpha2' => self::CR,
            'alpha3' => 'CRI',
            'numeric_code' => '188'
        ],
        self::CI => [
            'display_name' => 'Côte d\'Ivoire',
            'alpha2' => self::CI,
            'alpha3' => 'CIV',
            'numeric_code' => '384'
        ],
        self::HR => [
            'display_name' => 'Croatia',
            'alpha2' => self::HR,
            'alpha3' => 'HRV',
            'numeric_code' => '191'
        ],
        self::CU => [
            'display_name' => 'Cuba',
            'alpha2' => self::CU,
            'alpha3' => 'CUB',
            'numeric_code' => '192'
        ],
        self::CW => [
            'display_name' => 'Curaçao',
            'alpha2' => self::CW,
            'alpha3' => 'CUW',
            'numeric_code' => '531'
        ],
        self::CY => [
            'display_name' => 'Cyprus',
            'alpha2' => self::CY,
            'alpha3' => 'CYP',
            'numeric_code' => '196'
        ],
        self::CZ => [
            'display_name' => 'Czech Republic',
            'alpha2' => self::CZ,
            'alpha3' => 'CZE',
            'numeric_code' => '203'
        ],
        self::DK => [
            'display_name' => 'Denmark',
            'alpha2' => self::DK,
            'alpha3' => 'DNK',
            'numeric_code' => '208'
        ],
        self::DJ => [
            'display_name' => 'Djibouti',
            'alpha2' => self::DJ,
            'alpha3' => 'DJI',
            'numeric_code' => '262'
        ],
        self::DM => [
            'display_name' => 'Dominica',
            'alpha2' => self::DM,
            'alpha3' => 'DMA',
            'numeric_code' => '212'
        ],
        self::DO => [
            'display_name' => 'Dominican Republic',
            'alpha2' => self::DO,
            'alpha3' => 'DOM',
            'numeric_code' => '214'
        ],
        self::EC => [
            'display_name' => 'Ecuador',
            'alpha2' => self::EC,
            'alpha3' => 'ECU',
            'numeric_code' => '218'
        ],
        self::EG => [
            'display_name' => 'Egypt',
            'alpha2' => self::EG,
            'alpha3' => 'EGY',
            'numeric_code' => '818'
        ],
        self::SV => [
            'display_name' => 'El Salvador',
            'alpha2' => self::SV,
            'alpha3' => 'SLV',
            'numeric_code' => '222'
        ],
        self::GQ => [
            'display_name' => 'Equatorial Guinea',
            'alpha2' => self::GQ,
            'alpha3' => 'GNQ',
            'numeric_code' => '226'
        ],
        self::ER => [
            'display_name' => 'Eritrea',
            'alpha2' => self::ER,
            'alpha3' => 'ERI',
            'numeric_code' => '232'
        ],
        self::EE => [
            'display_name' => 'Estonia',
            'alpha2' => self::EE,
            'alpha3' => 'EST',
            'numeric_code' => '233'
        ],
        self::ET => [
            'display_name' => 'Ethiopia',
            'alpha2' => self::ET,
            'alpha3' => 'ETH',
            'numeric_code' => '231'
        ],
        self::FK => [
            'display_name' => 'Falkland Islands',
            'alpha2' => self::FK,
            'alpha3' => 'FLK',
            'numeric_code' => '238'
        ],
        self::FO => [
            'display_name' => 'Faroe Islands',
            'alpha2' => self::FO,
            'alpha3' => 'FRO',
            'numeric_code' => '234'
        ],
        self::FJ => [
            'display_name' => 'Fiji',
            'alpha2' => self::FJ,
            'alpha3' => 'FJI',
            'numeric_code' => '242'
        ],
        self::FI => [
            'display_name' => 'Finland',
            'alpha2' => self::FI,
            'alpha3' => 'FIN',
            'numeric_code' => '246'
        ],
        self::FR => [
            'display_name' => 'France',
            'alpha2' => self::FR,
            'alpha3' => 'FRA',
            'numeric_code' => '250'
        ],
        self::GF => [
            'display_name' => 'French Guiana',
            'alpha2' => self::GF,
            'alpha3' => 'GUF',
            'numeric_code' => '254'
        ],
        self::PF => [
            'display_name' => 'French Polynesia',
            'alpha2' => self::PF,
            'alpha3' => 'PYF',
            'numeric_code' => '258'
        ],
        self::TF => [
            'display_name' => 'French Southern Lands',
            'alpha2' => self::TF,
            'alpha3' => 'ATF',
            'numeric_code' => '260'
        ],
        self::GA => [
            'display_name' => 'Gabon',
            'alpha2' => self::GA,
            'alpha3' => 'GAB',
            'numeric_code' => '266'
        ],
        self::GM => [
            'display_name' => 'Gambia',
            'alpha2' => self::GM,
            'alpha3' => 'GMB',
            'numeric_code' => '270'
        ],
        self::GE => [
            'display_name' => 'Georgia',
            'alpha2' => self::GE,
            'alpha3' => 'GEO',
            'numeric_code' => '268'
        ],
        self::DE => [
            'display_name' => 'Germany',
            'alpha2' => self::DE,
            'alpha3' => 'DEU',
            'numeric_code' => '276'
        ],
        self::GH => [
            'display_name' => 'Ghana',
            'alpha2' => self::GH,
            'alpha3' => 'GHA',
            'numeric_code' => '288'
        ],
        self::GI => [
            'display_name' => 'Gibraltar',
            'alpha2' => self::GI,
            'alpha3' => 'GIB',
            'numeric_code' => '292'
        ],
        self::GR => [
            'display_name' => 'Greece',
            'alpha2' => self::GR,
            'alpha3' => 'GRC',
            'numeric_code' => '300'
        ],
        self::GL => [
            'display_name' => 'Greenland',
            'alpha2' => self::GL,
            'alpha3' => 'GRL',
            'numeric_code' => '304'
        ],
        self::GD => [
            'display_name' => 'Grenada',
            'alpha2' => self::GD,
            'alpha3' => 'GRD',
            'numeric_code' => '308'
        ],
        self::GP => [
            'display_name' => 'Guadeloupe',
            'alpha2' => self::GP,
            'alpha3' => 'GLP',
            'numeric_code' => '312'
        ],
        self::GU => [
            'display_name' => 'Guam',
            'alpha2' => self::GU,
            'alpha3' => 'GUM',
            'numeric_code' => '316'
        ],
        self::GT => [
            'display_name' => 'Guatemala',
            'alpha2' => self::GT,
            'alpha3' => 'GTM',
            'numeric_code' => '320'
        ],
        self::GG => [
            'display_name' => 'Guernsey',
            'alpha2' => self::GG,
            'alpha3' => 'GGY',
            'numeric_code' => '831'
        ],
        self::GN => [
            'display_name' => 'Guinea',
            'alpha2' => self::GN,
            'alpha3' => 'GIN',
            'numeric_code' => '324'
        ],
        self::GW => [
            'display_name' => 'Guinea-Bissau',
            'alpha2' => self::GW,
            'alpha3' => 'GNB',
            'numeric_code' => '624'
        ],
        self::GY => [
            'display_name' => 'Guyana',
            'alpha2' => self::GY,
            'alpha3' => 'GUY',
            'numeric_code' => '328'
        ],
        self::HT => [
            'display_name' => 'Haiti',
            'alpha2' => self::HT,
            'alpha3' => 'HTI',
            'numeric_code' => '332'
        ],
        self::HM => [
            'display_name' => 'Heard and McDonald Islands',
            'alpha2' => self::HM,
            'alpha3' => 'HMD',
            'numeric_code' => '334'
        ],
        self::HN => [
            'display_name' => 'Honduras',
            'alpha2' => self::HN,
            'alpha3' => 'HND',
            'numeric_code' => '340'
        ],
        self::HK => [
            'display_name' => 'Hong Kong',
            'alpha2' => self::HK,
            'alpha3' => 'HKG',
            'numeric_code' => '344'
        ],
        self::HU => [
            'display_name' => 'Hungary',
            'alpha2' => self::HU,
            'alpha3' => 'HUN',
            'numeric_code' => '348'
        ],
        self::IS => [
            'display_name' => 'Iceland',
            'alpha2' => self::IS,
            'alpha3' => 'ISL',
            'numeric_code' => '352'
        ],
        self::IN => [
            'display_name' => 'India',
            'alpha2' => self::IN,
            'alpha3' => 'IND',
            'numeric_code' => '356'
        ],
        self::ID => [
            'display_name' => 'Indonesia',
            'alpha2' => self::ID,
            'alpha3' => 'IDN',
            'numeric_code' => '360'
        ],
        self::IR => [
            'display_name' => 'Iran',
            'alpha2' => self::IR,
            'alpha3' => 'IRN',
            'numeric_code' => '364'
        ],
        self::IQ => [
            'display_name' => 'Iraq',
            'alpha2' => self::IQ,
            'alpha3' => 'IRQ',
            'numeric_code' => '368'
        ],
        self::IE => [
            'display_name' => 'Ireland',
            'alpha2' => self::IE,
            'alpha3' => 'IRL',
            'numeric_code' => '372'
        ],
        self::IM => [
            'display_name' => 'Isle of Man',
            'alpha2' => self::IM,
            'alpha3' => 'IMN',
            'numeric_code' => '833'
        ],
        self::IL => [
            'display_name' => 'Israel',
            'alpha2' => self::IL,
            'alpha3' => 'ISR',
            'numeric_code' => '376'
        ],
        self::IT => [
            'display_name' => 'Italy',
            'alpha2' => self::IT,
            'alpha3' => 'ITA',
            'numeric_code' => '380'
        ],
        self::JM => [
            'display_name' => 'Jamaica',
            'alpha2' => self::JM,
            'alpha3' => 'JAM',
            'numeric_code' => '388'
        ],
        self::JP => [
            'display_name' => 'Japan',
            'alpha2' => self::JP,
            'alpha3' => 'JPN',
            'numeric_code' => '392'
        ],
        self::JE => [
            'display_name' => 'Jersey',
            'alpha2' => self::JE,
            'alpha3' => 'JEY',
            'numeric_code' => '832'
        ],
        self::JO => [
            'display_name' => 'Jordan',
            'alpha2' => self::JO,
            'alpha3' => 'JOR',
            'numeric_code' => '400'
        ],
        self::KZ => [
            'display_name' => 'Kazakhstan',
            'alpha2' => self::KZ,
            'alpha3' => 'KAZ',
            'numeric_code' => '398'
        ],
        self::KE => [
            'display_name' => 'Kenya',
            'alpha2' => self::KE,
            'alpha3' => 'KEN',
            'numeric_code' => '404'
        ],
        self::KI => [
            'display_name' => 'Kiribati',
            'alpha2' => self::KI,
            'alpha3' => 'KIR',
            'numeric_code' => '296'
        ],
        self::KP => [
            'display_name' => 'Korea, North',
            'alpha2' => self::KP,
            'alpha3' => 'PRK',
            'numeric_code' => '408'
        ],
        self::KR => [
            'display_name' => 'Korea, South',
            'alpha2' => self::KR,
            'alpha3' => 'KOR',
            'numeric_code' => '410'
        ],
        self::KW => [
            'display_name' => 'Kuwait',
            'alpha2' => self::KW,
            'alpha3' => 'KWT',
            'numeric_code' => '414'
        ],
        self::KG => [
            'display_name' => 'Kyrgyzstan',
            'alpha2' => self::KG,
            'alpha3' => 'KGZ',
            'numeric_code' => '417'
        ],
        self::LA => [
            'display_name' => 'Laos',
            'alpha2' => self::LA,
            'alpha3' => 'LAO',
            'numeric_code' => '418'
        ],
        self::LV => [
            'display_name' => 'Latvia',
            'alpha2' => self::LV,
            'alpha3' => 'LVA',
            'numeric_code' => '428'
        ],
        self::LB => [
            'display_name' => 'Lebanon',
            'alpha2' => self::LB,
            'alpha3' => 'LBN',
            'numeric_code' => '422'
        ],
        self::LS => [
            'display_name' => 'Lesotho',
            'alpha2' => self::LS,
            'alpha3' => 'LSO',
            'numeric_code' => '426'
        ],
        self::LR => [
            'display_name' => 'Liberia',
            'alpha2' => self::LR,
            'alpha3' => 'LBR',
            'numeric_code' => '430'
        ],
        self::LY => [
            'display_name' => 'Libya',
            'alpha2' => self::LY,
            'alpha3' => 'LBY',
            'numeric_code' => '434'
        ],
        self::LI => [
            'display_name' => 'Liechtenstein',
            'alpha2' => self::LI,
            'alpha3' => 'LIE',
            'numeric_code' => '438'
        ],
        self::LT => [
            'display_name' => 'Lithuania',
            'alpha2' => self::LT,
            'alpha3' => 'LTU',
            'numeric_code' => '440'
        ],
        self::LU => [
            'display_name' => 'Luxembourg',
            'alpha2' => self::LU,
            'alpha3' => 'LUX',
            'numeric_code' => '442'
        ],
        self::MO => [
            'display_name' => 'Macau',
            'alpha2' => self::MO,
            'alpha3' => 'MOC',
            'numeric_code' => '446'
        ],
        self::MK => [
            'display_name' => 'Macedonia',
            'alpha2' => self::MK,
            'alpha3' => 'MKD',
            'numeric_code' => '807'
        ],
        self::MG => [
            'display_name' => 'Madagascar',
            'alpha2' => self::MG,
            'alpha3' => 'MDG',
            'numeric_code' => '450'
        ],
        self::MW => [
            'display_name' => 'Malawi',
            'alpha2' => self::MW,
            'alpha3' => 'MWI',
            'numeric_code' => '454'
        ],
        self::MY => [
            'display_name' => 'Malaysia',
            'alpha2' => self::MY,
            'alpha3' => 'MYS',
            'numeric_code' => '458'
        ],
        self::MV => [
            'display_name' => 'Maldives',
            'alpha2' => self::MV,
            'alpha3' => 'MDV',
            'numeric_code' => '462'
        ],
        self::ML => [
            'display_name' => 'Mali',
            'alpha2' => self::ML,
            'alpha3' => 'MLI',
            'numeric_code' => '466'
        ],
        self::MT => [
            'display_name' => 'Malta',
            'alpha2' => self::MT,
            'alpha3' => 'MLT',
            'numeric_code' => '470'
        ],
        self::MH => [
            'display_name' => 'Marshall Islands',
            'alpha2' => self::MH,
            'alpha3' => 'MHL',
            'numeric_code' => '584'
        ],
        self::MQ => [
            'display_name' => 'Martinique',
            'alpha2' => self::MQ,
            'alpha3' => 'MTQ',
            'numeric_code' => '474'
        ],
        self::MR => [
            'display_name' => 'Mauritania',
            'alpha2' => self::MR,
            'alpha3' => 'MRT',
            'numeric_code' => '478'
        ],
        self::MU => [
            'display_name' => 'Mauritius',
            'alpha2' => self::MU,
            'alpha3' => 'MUS',
            'numeric_code' => '480'
        ],
        self::YT => [
            'display_name' => 'Mayotte',
            'alpha2' => self::YT,
            'alpha3' => 'MYT',
            'numeric_code' => '175'
        ],
        self::MX => [
            'display_name' => 'Mexico',
            'alpha2' => self::MX,
            'alpha3' => 'MEX',
            'numeric_code' => '484'
        ],
        self::FM => [
            'display_name' => 'Micronesia',
            'alpha2' => self::FM,
            'alpha3' => 'FSM',
            'numeric_code' => '583'
        ],
        self::MD => [
            'display_name' => 'Moldova',
            'alpha2' => self::MD,
            'alpha3' => 'MDA',
            'numeric_code' => '498'
        ],
        self::MC => [
            'display_name' => 'Monaco',
            'alpha2' => self::MC,
            'alpha3' => 'MCO',
            'numeric_code' => '492'
        ],
        self::MN => [
            'display_name' => 'Mongolia',
            'alpha2' => self::MN,
            'alpha3' => 'MNG',
            'numeric_code' => '496'
        ],
        self::ME => [
            'display_name' => 'Montenegro',
            'alpha2' => self::ME,
            'alpha3' => 'MNE',
            'numeric_code' => '499'
        ],
        self::MS => [
            'display_name' => 'Montserrat',
            'alpha2' => self::MS,
            'alpha3' => 'MSR',
            'numeric_code' => '500'
        ],
        self::MA => [
            'display_name' => 'Morocco',
            'alpha2' => self::MA,
            'alpha3' => 'MAR',
            'numeric_code' => '504'
        ],
        self::MZ => [
            'display_name' => 'Mozambique',
            'alpha2' => self::MZ,
            'alpha3' => 'MOZ',
            'numeric_code' => '508'
        ],
        self::MM => [
            'display_name' => 'Myanmar',
            'alpha2' => self::MM,
            'alpha3' => 'MMR',
            'numeric_code' => '104'
        ],
        self::NA => [
            'display_name' => 'Namibia',
            'alpha2' => self::NA,
            'alpha3' => 'NAM',
            'numeric_code' => '516'
        ],
        self::NR => [
            'display_name' => 'Nauru',
            'alpha2' => self::NR,
            'alpha3' => 'NRU',
            'numeric_code' => '520'
        ],
        self::NP => [
            'display_name' => 'Nepal',
            'alpha2' => self::NP,
            'alpha3' => 'NPL',
            'numeric_code' => '524'
        ],
        self::NL => [
            'display_name' => 'Netherlands',
            'alpha2' => self::NL,
            'alpha3' => 'NLD',
            'numeric_code' => '528'
        ],
        self::AN => [
            'display_name' => 'Netherlands Antilles',
            'alpha2' => self::AN,
            'alpha3' => 'ANT',
            'numeric_code' => '530'
        ],
        self::NC => [
            'display_name' => 'New Caledonia',
            'alpha2' => self::NC,
            'alpha3' => 'NCL',
            'numeric_code' => '540'
        ],
        self::NZ => [
            'display_name' => 'New Zealand',
            'alpha2' => self::NZ,
            'alpha3' => 'NZL',
            'numeric_code' => '554'
        ],
        self::NI => [
            'display_name' => 'Nicaragua',
            'alpha2' => self::NI,
            'alpha3' => 'NIC',
            'numeric_code' => '558'
        ],
        self::NE => [
            'display_name' => 'Niger',
            'alpha2' => self::NE,
            'alpha3' => 'NER',
            'numeric_code' => '562'
        ],
        self::NG => [
            'display_name' => 'Nigeria',
            'alpha2' => self::NG,
            'alpha3' => 'NGA',
            'numeric_code' => '566'
        ],
        self::NU => [
            'display_name' => 'Niue',
            'alpha2' => self::NU,
            'alpha3' => 'NIU',
            'numeric_code' => '570'
        ],
        self::NF => [
            'display_name' => 'Norfolk Island',
            'alpha2' => self::NF,
            'alpha3' => 'NFK',
            'numeric_code' => '574'
        ],
        self::MP => [
            'display_name' => 'Northern Mariana Islands',
            'alpha2' => self::MP,
            'alpha3' => 'MNP',
            'numeric_code' => '580'
        ],
        self::NO => [
            'display_name' => 'Norway',
            'alpha2' => self::NO,
            'alpha3' => 'NOR',
            'numeric_code' => '578'
        ],
        self::OM => [
            'display_name' => 'Oman',
            'alpha2' => self::OM,
            'alpha3' => 'OMN',
            'numeric_code' => '512'
        ],
        self::PK => [
            'display_name' => 'Pakistan',
            'alpha2' => self::PK,
            'alpha3' => 'PAK',
            'numeric_code' => '586'
        ],
        self::PW => [
            'display_name' => 'Palau',
            'alpha2' => self::PW,
            'alpha3' => 'PLW',
            'numeric_code' => '585'
        ],
        self::PS => [
            'display_name' => 'Palestine',
            'alpha2' => self::PS,
            'alpha3' => 'PSE',
            'numeric_code' => '275'
        ],
        self::PA => [
            'display_name' => 'Panama',
            'alpha2' => self::PA,
            'alpha3' => 'PAN',
            'numeric_code' => '591'
        ],
        self::PG => [
            'display_name' => 'Papua New Guinea',
            'alpha2' => self::PG,
            'alpha3' => 'PNG',
            'numeric_code' => '598'
        ],
        self::PY => [
            'display_name' => 'Paraguay',
            'alpha2' => self::PY,
            'alpha3' => 'PRY',
            'numeric_code' => '600'
        ],
        self::PE => [
            'display_name' => 'Peru',
            'alpha2' => self::PE,
            'alpha3' => 'PER',
            'numeric_code' => '604'
        ],
        self::PH => [
            'display_name' => 'Philippines',
            'alpha2' => self::PH,
            'alpha3' => 'PHL',
            'numeric_code' => '608'
        ],
        self::PN => [
            'display_name' => 'Pitcairn',
            'alpha2' => self::PN,
            'alpha3' => 'PCN',
            'numeric_code' => '612'
        ],
        self::PL => [
            'display_name' => 'Poland',
            'alpha2' => self::PL,
            'alpha3' => 'POL',
            'numeric_code' => '616'
        ],
        self::PT => [
            'display_name' => 'Portugal',
            'alpha2' => self::PT,
            'alpha3' => 'PRT',
            'numeric_code' => '620'
        ],
        self::PR => [
            'display_name' => 'Puerto Rico',
            'alpha2' => self::PR,
            'alpha3' => 'PRI',
            'numeric_code' => '630'
        ],
        self::QA => [
            'display_name' => 'Qatar',
            'alpha2' => self::QA,
            'alpha3' => 'QAT',
            'numeric_code' => '634'
        ],
        self::RE => [
            'display_name' => 'Reunion',
            'alpha2' => self::RE,
            'alpha3' => 'REU',
            'numeric_code' => '638'
        ],
        self::RO => [
            'display_name' => 'Romania',
            'alpha2' => self::RO,
            'alpha3' => 'ROU',
            'numeric_code' => '642'
        ],
        self::RU => [
            'display_name' => 'Russian Federation',
            'alpha2' => self::RU,
            'alpha3' => 'RUS',
            'numeric_code' => '643'
        ],
        self::RW => [
            'display_name' => 'Rwanda',
            'alpha2' => self::RW,
            'alpha3' => 'RWA',
            'numeric_code' => '646'
        ],
        self::BL => [
            'display_name' => 'Saint Barthélemy',
            'alpha2' => self::BL,
            'alpha3' => 'BLM',
            'numeric_code' => '652'
        ],
        self::SH => [
            'display_name' => 'Saint Helena',
            'alpha2' => self::SH,
            'alpha3' => 'SHN',
            'numeric_code' => '654'
        ],
        self::KN => [
            'display_name' => 'Saint Kitts and Nevis',
            'alpha2' => self::KN,
            'alpha3' => 'KNA',
            'numeric_code' => '659'
        ],
        self::LC => [
            'display_name' => 'Saint Lucia',
            'alpha2' => self::LC,
            'alpha3' => 'LCA',
            'numeric_code' => '662'
        ],
        self::MF => [
            'display_name' => 'Saint Martin (French part)',
            'alpha2' => self::MF,
            'alpha3' => 'MAF',
            'numeric_code' => '663'
        ],
        self::PM => [
            'display_name' => 'Saint Pierre and Miquelon',
            'alpha2' => self::PM,
            'alpha3' => 'SPM',
            'numeric_code' => '666'
        ],
        self::VC => [
            'display_name' => 'Saint Vincent and the Grenadines',
            'alpha2' => self::VC,
            'alpha3' => 'VCT',
            'numeric_code' => '670'
        ],
        self::WS => [
            'display_name' => 'Samoa',
            'alpha2' => self::WS,
            'alpha3' => 'WSM',
            'numeric_code' => '882'
        ],
        self::SM => [
            'display_name' => 'San Marino',
            'alpha2' => self::SM,
            'alpha3' => 'SMR',
            'numeric_code' => '674'
        ],
        self::ST => [
            'display_name' => 'Sao Tome and Principe',
            'alpha2' => self::ST,
            'alpha3' => 'STP',
            'numeric_code' => '678'
        ],
        self::SA => [
            'display_name' => 'Saudi Arabia',
            'alpha2' => self::SA,
            'alpha3' => 'SAU',
            'numeric_code' => '682'
        ],
        self::SN => [
            'display_name' => 'Senegal',
            'alpha2' => self::SN,
            'alpha3' => 'SEN',
            'numeric_code' => '686'
        ],
        self::RS => [
            'display_name' => 'Serbia',
            'alpha2' => self::RS,
            'alpha3' => 'SRB',
            'numeric_code' => '688'
        ],
        self::SC => [
            'display_name' => 'Seychelles',
            'alpha2' => self::SC,
            'alpha3' => 'SYC',
            'numeric_code' => '690'
        ],
        self::SL => [
            'display_name' => 'Sierra Leone',
            'alpha2' => self::SL,
            'alpha3' => 'SLE',
            'numeric_code' => '694'
        ],
        self::SG => [
            'display_name' => 'Singapore',
            'alpha2' => self::SG,
            'alpha3' => 'SGP',
            'numeric_code' => '702'
        ],
        self::SX => [
            'display_name' => 'Sint Maarten (Dutch part)',
            'alpha2' => self::SX,
            'alpha3' => 'SXM',
            'numeric_code' => '534'
        ],
        self::SK => [
            'display_name' => 'Slovakia',
            'alpha2' => self::SK,
            'alpha3' => 'SVK',
            'numeric_code' => '703'
        ],
        self::SI => [
            'display_name' => 'Slovenia',
            'alpha2' => self::SI,
            'alpha3' => 'SVN',
            'numeric_code' => '705'
        ],
        self::SB => [
            'display_name' => 'Solomon Islands',
            'alpha2' => self::SB,
            'alpha3' => 'SLB',
            'numeric_code' => '090'
        ],
        self::SO => [
            'display_name' => 'Somalia',
            'alpha2' => self::SO,
            'alpha3' => 'SOM',
            'numeric_code' => '706'
        ],
        self::ZA => [
            'display_name' => 'South Africa',
            'alpha2' => self::ZA,
            'alpha3' => 'ZAF',
            'numeric_code' => '710'
        ],
        self::GS => [
            'display_name' => 'South Georgia and South Sandwich Islands',
            'alpha2' => self::GS,
            'alpha3' => 'SGS',
            'numeric_code' => '239'
        ],
        self::SS => [
            'display_name' => 'South Sudan',
            'alpha2' => self::SS,
            'alpha3' => 'SSD',
            'numeric_code' => '728'
        ],
        self::ES => [
            'display_name' => 'Spain',
            'alpha2' => self::ES,
            'alpha3' => 'ESP',
            'numeric_code' => '724'
        ],
        self::LK => [
            'display_name' => 'Sri Lanka',
            'alpha2' => self::LK,
            'alpha3' => 'LKA',
            'numeric_code' => '144'
        ],
        self::SD => [
            'display_name' => 'Sudan',
            'alpha2' => self::SD,
            'alpha3' => 'SDN',
            'numeric_code' => '736'
        ],
        self::SR => [
            'display_name' => 'Suriname',
            'alpha2' => self::SR,
            'alpha3' => 'SUR',
            'numeric_code' => '740'
        ],
        self::SJ => [
            'display_name' => 'Svalbard and Jan Mayen Islands',
            'alpha2' => self::SJ,
            'alpha3' => 'SJM',
            'numeric_code' => '744'
        ],
        self::SZ => [
            'display_name' => 'Swaziland',
            'alpha2' => self::SZ,
            'alpha3' => 'SWZ',
            'numeric_code' => '748'
        ],
        self::SE => [
            'display_name' => 'Sweden',
            'alpha2' => self::SE,
            'alpha3' => 'SWE',
            'numeric_code' => '752'
        ],
        self::CH => [
            'display_name' => 'Switzerland',
            'alpha2' => self::CH,
            'alpha3' => 'CHE',
            'numeric_code' => '756'
        ],
        self::SY => [
            'display_name' => 'Syria',
            'alpha2' => self::SY,
            'alpha3' => 'SYR',
            'numeric_code' => '760'
        ],
        self::TW => [
            'display_name' => 'Taiwan',
            'alpha2' => self::TW,
            'alpha3' => 'TWN',
            'numeric_code' => '158'
        ],
        self::TJ => [
            'display_name' => 'Tajikistan',
            'alpha2' => self::TJ,
            'alpha3' => 'TJK',
            'numeric_code' => '762'
        ],
        self::TZ => [
            'display_name' => 'Tanzania',
            'alpha2' => self::TZ,
            'alpha3' => 'TZA',
            'numeric_code' => '834'
        ],
        self::TH => [
            'display_name' => 'Thailand',
            'alpha2' => self::TH,
            'alpha3' => 'THA',
            'numeric_code' => '764'
        ],
        self::TL => [
            'display_name' => 'Timor-Leste',
            'alpha2' => self::TL,
            'alpha3' => 'TLS',
            'numeric_code' => '626'
        ],
        self::TG => [
            'display_name' => 'Togo',
            'alpha2' => self::TG,
            'alpha3' => 'TGO',
            'numeric_code' => '768'
        ],
        self::TK => [
            'display_name' => 'Tokelau',
            'alpha2' => self::TK,
            'alpha3' => 'TKL',
            'numeric_code' => '772'
        ],
        self::TO => [
            'display_name' => 'Tonga',
            'alpha2' => self::TO,
            'alpha3' => 'TON',
            'numeric_code' => '776'
        ],
        self::TT => [
            'display_name' => 'Trinidad and Tobago',
            'alpha2' => self::TT,
            'alpha3' => 'TTO',
            'numeric_code' => '780'
        ],
        self::TN => [
            'display_name' => 'Tunisia',
            'alpha2' => self::TN,
            'alpha3' => 'TUN',
            'numeric_code' => '788'
        ],
        self::TR => [
            'display_name' => 'Turkey',
            'alpha2' => self::TR,
            'alpha3' => 'TUR',
            'numeric_code' => '792'
        ],
        self::TM => [
            'display_name' => 'Turkmenistan',
            'alpha2' => self::TM,
            'alpha3' => 'TKM',
            'numeric_code' => '795'
        ],
        self::TC => [
            'display_name' => 'Turks and Caicos Islands',
            'alpha2' => self::TC,
            'alpha3' => 'TCA',
            'numeric_code' => '796'
        ],
        self::TV => [
            'display_name' => 'Tuvalu',
            'alpha2' => self::TV,
            'alpha3' => 'TUV',
            'numeric_code' => '798'
        ],
        self::UG => [
            'display_name' => 'Uganda',
            'alpha2' => self::UG,
            'alpha3' => 'UGA',
            'numeric_code' => '800'
        ],
        self::UA => [
            'display_name' => 'Ukraine',
            'alpha2' => self::UA,
            'alpha3' => 'UKR',
            'numeric_code' => '804'
        ],
        self::AE => [
            'display_name' => 'United Arab Emirates',
            'alpha2' => self::AE,
            'alpha3' => 'ARE',
            'numeric_code' => '784'
        ],
        self::GB => [
            'display_name' => 'United Kingdom',
            'alpha2' => self::GB,
            'alpha3' => 'GBR',
            'numeric_code' => '826'
        ],
        self::UM => [
            'display_name' => 'United States Minor Outlying Islands',
            'alpha2' => self::UM,
            'alpha3' => 'UMI',
            'numeric_code' => '581'
        ],
        self::US => [
            'display_name' => 'United States of America',
            'alpha2' => self::US,
            'alpha3' => 'USA',
            'numeric_code' => '840'
        ],
        self::UY => [
            'display_name' => 'Uruguay',
            'alpha2' => self::UY,
            'alpha3' => 'URY',
            'numeric_code' => '858'
        ],
        self::UZ => [
            'display_name' => 'Uzbekistan',
            'alpha2' => self::UZ,
            'alpha3' => 'UZB',
            'numeric_code' => '860'
        ],
        self::VU => [
            'display_name' => 'Vanuatu',
            'alpha2' => self::VU,
            'alpha3' => 'VUT',
            'numeric_code' => '548'
        ],
        self::VA => [
            'display_name' => 'Vatican City',
            'alpha2' => self::VA,
            'alpha3' => 'VAT',
            'numeric_code' => '336'
        ],
        self::VE => [
            'display_name' => 'Venezuela',
            'alpha2' => self::VE,
            'alpha3' => 'VEN',
            'numeric_code' => '862'
        ],
        self::VN => [
            'display_name' => 'Vietnam',
            'alpha2' => self::VN,
            'alpha3' => 'VNM',
            'numeric_code' => '704'
        ],
        self::VG => [
            'display_name' => 'Virgin Islands, British',
            'alpha2' => self::VG,
            'alpha3' => 'VGB',
            'numeric_code' => '092'
        ],
        self::VI => [
            'display_name' => 'Virgin Islands, U.S.',
            'alpha2' => self::VI,
            'alpha3' => 'VIR',
            'numeric_code' => '850'
        ],
        self::WF => [
            'display_name' => 'Wallis and Futuna Islands',
            'alpha2' => self::WF,
            'alpha3' => 'WLF',
            'numeric_code' => '876'
        ],
        self::EH => [
            'display_name' => 'Western Sahara',
            'alpha2' => self::EH,
            'alpha3' => 'ESH',
            'numeric_code' => '732'
        ],
        self::YE => [
            'display_name' => 'Yemen',
            'alpha2' => self::YE,
            'alpha3' => 'YEM',
            'numeric_code' => '887'
        ],
        self::ZM => [
            'display_name' => 'Zambia',
            'alpha2' => self::ZM,
            'alpha3' => 'ZMB',
            'numeric_code' => '894'
        ],
        self::ZW => [
            'display_name' => 'Zimbabwe',
            'alpha2' => self::ZW,
            'alpha3' => 'ZWE',
            'numeric_code' => '716'
        ]
    ];

    /** @var array */
    private static $countryCodes;

    /**
     * @var string
     */
    private $countryCode;

    /**
     * Country constructor.
     * @param string $countryCode
     */
    public function __construct($countryCode)
    {
        parent::__construct($countryCode);

        $this->countryCode = $countryCode;
    }

    /**
     * @param string $alpha3
     * @return Country
     */
    public static function fromAlpha3(string $alpha3): self
    {
        foreach (self::toArray() as $country) {
            if ($country['alpha3'] === $alpha3) {
                return new self($country['alpha2']);
            }
        }

        throw new \BadMethodCallException(sprintf('Unknown enum key "%s"', $alpha3));
    }

    /**
     * @return array the list of configured countries
     */
    public static function getCountries()
    {
        return self::$values;
    }

    /**
     * @return array
     */
    public static function getCountryCodes()
    {
        if (!self::$countryCodes) {
            foreach (self::$values as $countryCode => $country) {
                self::$countryCodes[$countryCode] = $country['display_name'];
            }
        }

        return self::$countryCodes;
    }

    /**
     * Returns the ISO 4217 country code of this country.
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Returns two letter abbreviation of the country.
     *
     * @return string
     */
    public function getAlpha2()
    {
        return self::$values[$this->countryCode]['alpha2'];
    }

    /**
     * Returns three letter abbreviation of the country.
     *
     * @return string
     */
    public function getAlpha3()
    {
        return self::$values[$this->countryCode]['alpha3'];
    }

    /**
     * Returns the ISO 4217 numeric code of this country.
     *
     * @return string
     */
    public function getNumericCode()
    {
        return self::$values[$this->countryCode]['numeric_code'];
    }
}
