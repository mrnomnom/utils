PHP Enum implementation
=======================

Provides the ability to create enumeration objects in PHP.

# Contributing

* **Values in shared enum must only be appended.** Do not rename them or change structure

# Usage examples

## Enum

```php
/**
 * @method static JANUARY()
 * @method static FEBRUARY()
 */
class Month extends TGF\Util\Enum\Enum
{
    const JANUARY = 'january';
    const FEBRUARY = 'february';
}
```

## EnumOfArrays

```php
/**
 * @method static Country LT()
 */
class Country extends TGF\Util\Enum\EnumOfArrays
{
    /**
     * @var array
     */
    private static $values = [
        'LT' => [
            'display_name' => 'Lithuania',
            'alpha2' => 'LT',
            'alpha3' => 'LTL',
        ],
    ];
}
```

# Shared enums

Library provides some shared enums:

* TGF\Util\Enum\Financial\Currency
* TGF\Util\Enum\Regional\Country
* TGF\Util\Enum\Regional\Nationality
