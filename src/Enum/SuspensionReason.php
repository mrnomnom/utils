<?php

namespace TGF\Util\Enum;

class SuspensionReason extends Enum
{
    //Manual rules
    const MISMATCH = 'MISMATCH';
    const REFUND = 'REFUND';
    const POF_AND_QS = 'POF_AND_QS';
    const POP = 'POP';
    const RETURNED = 'RETURNED';
    const RD = 'RD';
    const WRONG_ACCOUNT = 'WRONG_ACCOUNT';
    const WRONG_CURRENCY = 'WRONG_CURRENCY';
    const NOT_FOUND_OR_CLOSED_ACCOUNT = 'NOT_FOUND_OR_CLOSED_ACCOUNT';
    const REJECTED = 'REJECTED';
    const CUSTOMER_NOT_FOUND = 'CUSTOMER_NOT_FOUND';
    const CARD_NOT_FOUND = 'CARD_NOT_FOUND';
    const BOOKING_HASH_IS_INVALID = 'BOOKING_HASH_IS_INVALID';
    const SEE_ADDITIONAL_DETAILS = 'SEE_ADDITIONAL_DETAILS';
    const AML_SANCTIONS_RECIPIENT_FAILED = 'AML_SANCTIONS_RECIPIENT_FAILED';
    const AML_SANCTIONS_SENDER_FAILED = 'AML_SANCTIONS_SENDER_FAILED';
    const BANK_IS_FACING_ISSUES = 'BANK_IS_FACING_ISSUES';
    const OUTSIDE_BUSINESS_HOURS = 'OUTSIDE_BUSINESS_HOURS';
    const INCORRECT_SENDER_DETAILS = 'INCORRECT_SENDER_DETAILS';

    //Semi auto rules
    const SANCTIONS_CHECK_FAILED = 'SANCTIONS_CHECK_FAILED';
    const BLUECASH_FAILED = 'BLUECASH_FAILED';

    //Auto rules
    const CANCELLED_BY_USER_RULE = "CANCELLED_BY_USER_RULE";
    const EXPIRED_RULE = "EXPIRED_RULE";
    const PROOF_OF_ADDRESS_RULE = "PROOF_OF_ADDRESS_RULE";
    const PROOF_OF_ID_RULE = "PROOF_OF_ID_RULE";
    const USER_AUTO_ON_HOLD_RULE = "USER_AUTO_ON_HOLD_RULE";
    const RECIPIENT_ACCOUNT_IS_ON_BLACKLIST_RULE = "RECIPIENT_ACCOUNT_IS_ON_BLACKLIST_RULE";
    const DAILY_AMOUNT_LIMIT_RULE = "DAILY_AMOUNT_LIMIT_RULE";
    const DAILY_TRANSACTION_LIMIT_RULE = "DAILY_TRANSACTION_LIMIT_RULE";
    const YEARLY_AMOUNT_LIMIT_RULE = "YEARLY_AMOUNT_LIMIT_RULE";
    const YEARLY_TRANSACTION_LIMIT_RULE = "YEARLY_TRANSACTION_LIMIT_RULE";
    const MULTIPLE_CARDS_FOR_TRANSACTION_RULE = "MULTIPLE_CARDS_FOR_TRANSACTION_RULE";
    const MULTIPLE_TRANSACTIONS_WITH_SAME_CARD_RULE = "MULTIPLE_TRANSACTIONS_WITH_SAME_CARD_RULE";
    const YEARLY_TURNOVER_EXCEEDS_INCOME_RULE = "YEARLY_TURNOVER_EXCEEDS_INCOME_RULE";
    const MULTIPLE_USERS_WITH_SAME_CARD_RULE = "MULTIPLE_USERS_WITH_SAME_CARD_RULE";
    const ECDD_CHECK_RULE = "ECDD_CHECK_RULE";
}
