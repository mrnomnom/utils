<?php

namespace TGF\Util\IncomingQueue;

use Aws\Sqs\SqsClient;
use TGF\Util\Logger\LoggerInterface;

class QueueFactory
{
    /**
     * @param LoggerInterface $logger
     * @param SqsClient       $queueClient
     * @param string          $queuePath
     *
     * @return Queue
     */
    public static function create(LoggerInterface $logger, SqsClient $queueClient, string $queuePath): Queue
    {
        return new Queue(
            $queueClient,
            $queuePath,
            $logger
        );
    }
}
