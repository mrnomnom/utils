<?php

namespace TGF\Util\IncomingQueue;

class Job
{
    /** @var string */
    private $receiptHandle;

    /** @var array */
    private $body;

    /** @var array */
    private $meta;

    /**
     * @param string $receiptHandle
     * @param array  $body
     * @param array  $meta
     */
    public function __construct(string $receiptHandle, array $body, array $meta = [])
    {
        $this->receiptHandle = $receiptHandle;
        $this->body = $body;
        $this->meta = $meta;
    }

    /**
     * @return string
     */
    public function getReceiptHandle()
    {
        return $this->receiptHandle;
    }

    /**
     * @return array
     */
    public function getBody(): array
    {
        return $this->body;
    }

    /**
     * @return array
     */
    public function getMeta(): array
    {
        return $this->meta;
    }

    /**
     * @return null|string
     */
    public function getTraceId()
    {
        return isset($this->meta['traceId']) ? $this->meta['traceId'] : null;
    }

    public function delete()
    {
    }
}
