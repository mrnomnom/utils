<?php

namespace TGF\Util\IncomingQueue;

use Aws\Sqs\SqsClient;
use TGF\Util\Logger\LoggerInterface;

class Queue
{
    /** @var SqsClient */
    private $sqsClient;

    /** @var string */
    private $queueUrl;

    /** @var LoggerInterface */
    private $logger;

    /**
     * @param SqsClient       $client
     * @param string          $queueUrl
     * @param LoggerInterface $logger
     */
    public function __construct(SqsClient $client, string $queueUrl, LoggerInterface $logger)
    {
        $this->sqsClient = $client;
        $this->queueUrl = $queueUrl;
        $this->logger = $logger;
    }

    /**
     * @param int $waitTimeInSeconds
     * @return Job[]
     */
    public function getJobsWithWaiting(int $waitTimeInSeconds)
    {
        return $this->getJobs(['WaitTimeSeconds' => $waitTimeInSeconds]);
    }

    /**
     * @param array $arguments
     * @return Job[]
     */
    public function getJobs(array $arguments = [])
    {
        $this->logger->debug("Attempting to retrieve jobs from '$this->queueUrl'");

        $defaultArguments = [
            'QueueUrl' => $this->queueUrl,
            'AttributeNames' => ['SentTimestamp'],
            'MaxNumberOfMessages' => 10,
        ];

        $sqsResult = $this
            ->sqsClient
            ->receiveMessage(
                array_replace_recursive($defaultArguments, $arguments)
            );

        if (empty($sqsResult['Messages'])) {
            return [];
        }

        $jobCount = count($sqsResult['Messages']);
        $this->logger->debug("Jobs retrieved: $jobCount");

        $jobs = [];

        foreach ($sqsResult['Messages'] as $message) {
            try {
                $this->logger->info("Trying to parse job");
                $jobBody = json_decode(json_decode($message['Body'], true)['Message'], true);

                if (empty($jobBody) || is_array($jobBody) == false) {
                    $this->logger->error("Invalid job body. " . var_export($message, true));
                    $this->deleteInvalidJob($message['ReceiptHandle']);

                    continue;
                }

                $jobs[] = new Job($message['ReceiptHandle'], $jobBody, $jobBody['meta'] ?? []);

                $this->logger->info("Jobs parsed: $jobCount");
            } catch (\Exception $e) {
                $this->logger->error("Jobs parse error:" . $e->getMessage());
            }
        }

        return $jobs;
    }

    /**
     * @param Job $job
     */
    public function delete(Job $job): void
    {
        $this->sqsClient->deleteMessage(
            [
                'QueueUrl' => $this->queueUrl,
                'ReceiptHandle' => $job->getReceiptHandle(),
            ]
        );
        $this->logger->info('Job successfully deleted.');
    }

    /**
     * @param string $receiptHandle
     */
    private function deleteInvalidJob($receiptHandle): void
    {
        $this->sqsClient->deleteMessage(
            [
                'QueueUrl' => $this->queueUrl,
                'ReceiptHandle' => $receiptHandle,
            ]
        );

        $this->logger->info('Invalid job deleted.');
    }
}
