<?php

namespace TGF\Util\IncomingQueue;

use Aws\Sqs\SqsClient;

class SqsClientFactory
{
    /**
     * @param string $region
     * @param string $accessKeyId
     * @param string $secretAccessKey
     *
     * @return SqsClient
     */
    public static function create(string $region, string $accessKeyId, string $secretAccessKey): SqsClient
    {
        return new SqsClient(
            [
                'version'     => 'latest',
                'region'      => $region,
                'credentials' => [
                    'key'    => $accessKeyId,
                    'secret' => $secretAccessKey,
                ],
            ]
        );
    }
}
