<?php

namespace spec\TGF\Util\Logger;

use PhpSpec\ObjectBehavior;
use TGF\Util\Logger\Formatter;

class FormatterSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith();
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Formatter::class);
    }

    function it_takes_message_from_exception()
    {
        $message = 'This is test message';
        $e = new \Exception($message);

        $this->format([$e])->shouldHaveKeyWithValue('message', $message);
        $this->format([$e])->shouldHaveKey('context');
    }

    function it_takes_exception_class_name_if_no_message_in_exception()
    {
        $e = new \Exception();

        $this->format([$e])->shouldHaveKeyWithValue('message', get_class($e));
    }

    function it_should_sanitize_context_data()
    {
        $this->format(['PREF', ['tookTime' => 'ą'[0]]])
             ->shouldHaveKeyWithValue(
                 'context',
                 ['_NON_SERIALIZABLE_' => 'YToxOntzOjg6InRvb2tUaW1lIjtzOjE6IsQiO30=']
             );
    }
}
