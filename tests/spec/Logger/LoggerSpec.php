<?php

namespace spec\TGF\Util\Logger;

use TGF\Util\Logger\Logger;
use PhpSpec\ObjectBehavior;
use TGF\Util\Logger\MetaProvider;

class LoggerSpec extends ObjectBehavior
{

    function let(\Monolog\Logger $logger, MetaProvider $metaProvider)
    {
        $metaProvider->getMeta()->willReturn([]);

        $this->beConstructedWith($logger, $metaProvider, []);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Logger::class);
    }

    function it_can_log_error_with_only_message()
    {
        $message = 'This is error message';
        $this->error($message);
    }

    function it_can_log_warning_with_only_message()
    {
        $message = 'This is warning message';
        $this->warning($message);
    }

    function it_can_log_info_with_only_message()
    {
        $message = 'This is info message';
        $this->info($message);
    }

    function it_can_log_debug_with_only_message()
    {
        $message = 'This is debug message';
        $this->debug($message);
    }

    function it_can_log_error_with_message_and_context_information_array()
    {
        $message = 'This is error message';
        $context = ['a' => 1, 'x' => 'John', 'more' => 'aa'];

        $this->error($message, $context);
    }

    function it_can_log_warning_with_message_and_context_information_array()
    {
        $message = 'This is warning message';
        $context = ['a' => 1, 'x' => 'John', 'more' => 'aa'];

        $this->warning($message, $context);
    }

    function it_can_log_info_with_message_and_context_information_array()
    {
        $errorText = 'This is info message';
        $context = ['a' => 1, 'x' => 'John', 'more' => 'aa'];

        $this->info($errorText, $context);
    }

    function it_can_log_debug_with_message_and_context_information_array()
    {
        $message = 'This is info message';
        $context = ['a' => 1, 'x' => 'John', 'more' => []];

        $this->debug($message, $context);
    }

    function it_accepts_exception_as_a_single_argument(\Exception $exception)
    {
        $this->error($exception);
    }
}
