TGF Utility classes
===================

This package collects various utility classes that can be shared between projects.

# Installation

Add repository configuration to composer.json:

```php
{
    "repositories": {
        "transfergo/utils": {
            "type": "vcs",
            "url": "https://bitbucket.org/transfergo/utils.git"
        }
    },
    "require": {
        "transfergo/utils": "^1.0"
    }
}
```

# Contributing

* Project uses semantic versioning rules to tag changes
* Note what changes were made in CHANGELOG.md file
